package com.myrest.restfulwebservice.base.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myrest.restfulwebservice.base.exception.UserNotFoundException;
import com.myrest.restfulwebservice.base.pojo.User;
import com.myrest.restfulwebservice.base.service.UserDao;

@RestController
@RequestMapping("/user")
public class UserResource {
	
	@Autowired
	private UserDao userDao;

	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}
	@GetMapping("/{id}")
	public User findById(@PathVariable("id") int id) {
		return userDao.findOne(id);
	}
	
	@PostMapping("/newUser")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
		User savedUser=userDao.postUser(user);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
	}
	
	 @DeleteMapping("/{id}")
     public void deleteUser(@PathVariable("id") int id) {
    	 User user=userDao.deleteById(id);
    	 if (user == null) {
    		 throw new UserNotFoundException("User Not Found With "+id);
    	 }
     }
	
}
