package com.myrest.restfulwebservice.base.exception;

import java.util.Date;

public class ExceptionResponse {

	private Date timeOfOccurance;

	private String message;

	private String tracedDetails;

	public ExceptionResponse() {

	}

	public ExceptionResponse(Date timeOfOccurance, String message, String tracedDetails) {
		super();
		this.timeOfOccurance = timeOfOccurance;
		this.message = message;
		this.tracedDetails = tracedDetails;
	}

	public Date getTimeOfOccurance() {
		return timeOfOccurance;
	}

	public void setTimeOfOccurance(Date timeOfOccurance) {
		this.timeOfOccurance = timeOfOccurance;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTracedDetails() {
		return tracedDetails;
	}

	public void setTracedDetails(String tracedDetails) {
		this.tracedDetails = tracedDetails;
	}

	
}
