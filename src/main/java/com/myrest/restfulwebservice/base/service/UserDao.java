package com.myrest.restfulwebservice.base.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.myrest.restfulwebservice.base.exception.UserNotFoundException;
import com.myrest.restfulwebservice.base.pojo.User;

@Component
public class UserDao {
	
	private static List<User> users=new ArrayList<User>();
	
	private static int userCount= 3;
	
	static {
		users.add(new User(1, "Zac", new Date()));
		users.add(new User(2, "Mark", new Date()));
		users.add(new User(3, "Louis", new Date()));
	}
	
	public List<User> getAllUsers(){
		return users;
	}
	
	public User postUser(User user) {
		if(user.getId() == null) {
			user.setId(++userCount);
		}
		users.add(user);
		return user;
	}
	
	public User findOne(int id) {
		Optional<User> matchedUser=users.stream().filter(user -> user.getId() == id).findAny();
		if(!matchedUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		return matchedUser.get();
	}
	
	public User deleteById(int id) {
		Iterator<User> userIterator=users.iterator();
		while (userIterator.hasNext()) {
			User user = userIterator.next();
			if(user.getId().intValue() == id) {
				userIterator.remove();
				return user;
			}
		}
		return null;
	}

}
